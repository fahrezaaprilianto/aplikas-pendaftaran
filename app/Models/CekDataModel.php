<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CekDataModel extends Model
{
    public function allData()
    {
    	 return DB::table('data_diri')->get();
    }

    public function detailData($id)
    {
    	
    	return DB::table('data_diri')->where('id', $id)->first();
    }

    // public function addData($data)
    // {
    // 	 DB::table('tbl_dosen')->insert($data);
    // }

    public function editData($id, $data)
    {
    	DB::table('data_diri')->where('id', $id)->update($data);
    }

    

    // public function deleteData($id_dosen)
    // {
    	
    // 	DB::table('tbl_dosen')->where('id_dosen', $id_dosen)->delete();
    // }
}
