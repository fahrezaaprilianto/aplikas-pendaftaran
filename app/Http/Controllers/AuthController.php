<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Auth;

class AuthController extends Controller
{
    public function login(){

    	return view('auth.login');
    }

    public function postLogin(Request $request){

    	if (Auth::attempt($request->only('email','password'))){
    		$id = Auth::id();
    		$User = new \App\User;
    		$get = $User::where('id', $id)->first();
    		$status = $get->status;

    		if ($status == "NOT_CONFIRM"){

    			return redirect('/confirm');
    		}elseif ($status == "MERAH"){

    			return redirect('/merah');
    		}elseif ($status == "KUNING"){

    			return redirect('/kuning');
    		}elseif ($status == "CHECKING"){

                return redirect('/send');
            }elseif ($status == "HIJAU"){

                return redirect('/dashboard');
            }else{
    			return redirect('/dashboard');
    		}

    	}
    	return redirect('/')->with('failed', 'Email/Password Salah!');
    }

    public function logout(){

    	Auth::logout();
    	return redirect('/');
    }

    public function register(){

    	return view('auth.register');	
    }

    public function postRegister(Request $request){

    $this->validate($request,[
    		'first_name' => 'required',
    		'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|min:8|same:password',
            ],
            [
            'first_name.required' => 'First Name Wajib Diisi',
            'last_name.required' => 'Last Name Wajib Diisi',	
            'email.unique' => 'Email Tidak Boleh Sama',
            'password_confirmation.same' => 'Password Tidak Sama',

                ]);
        $user = new \App\User;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->role = "user";
        $user->password = $request->password;
        $user->remember_token = Str::random(60);
        $user->status = "NOT_CONFIRM";
        $user->save();

        return redirect('/')->with('sukses', 'Create User');
     }


     public function confirm(){

    	return view('auth.confirm');	
     }
     public function postConfirm(Request $request){
    //  	$this->validate($request,[
    //         'lembaga' => 'required',
    //         'alamat' => 'required',
    //         'kabupaten' => 'required',
    //         'kota' => 'required',
    //         'provinsi' => 'required',
    //         'email' => 'required',
    //         'no_hp' => 'required',
    //         'srt_permohonan' => 'required|mimes:jpeg,jpg,png,pdf|max:10000',
    //         'srt_permohonankanwil' => 'required|mimes:jpeg,jpg,png,pdf|max:10000',
    //         'srt_keputusan' => 'required|mimes:jpeg,jpg,png,pdf|max:10000',
    //         'susunan_lembaga' => 'required|mimes:jpeg,jpg,png,pdf|max:10000',
    //         'pas_foto' => 'required|mimes:jpeg,jpg,png,pdf|max:10000',
    //         'foto_ktp' => 'required|mimes:jpeg,jpg,png,pdf|max:10000'
    // ],
    // [
        
    //     'lembaga.required' => 'Harus Diisi!',
    //     'alamat.required' => 'Harus Diisi!',
    //     'kabupaten.required' => 'Harus Diisi!',
    //     'kota.required' => 'Harus Diisi!',
    //     'provinsi.required' => 'Harus Diisi!',
    //     'email.required' => 'Harus Diisi!',
    //     'no_hp.required' => 'Harus Diisi!',
    //     'srt_permohonan.mimes' => 'Foto harus berformat JPEG/JPG/PNG/PDF',
    //     'srt_permohonan.max' => 'Foto harus kurang dari 10 MB',
    //     'srt_permohonankanwil.mimes' => 'Foto harus berformat JPEG/JPG/PNG/PDF',
    //     'srt_permohonankanwil.max' => 'Foto harus kurang dari 10 MB',
    //     'srt_keputusan.mimes' => 'Foto harus berformat JPEG/JPG/PNG/PDF',
    //     'srt_keputusan.max' => 'Foto harus kurang dari 10 MB',
    //     'susunan_lembaga.mimes' => 'Foto harus berformat JPEG/JPG/PNG/PDF',
    //     'susunan_lembaga.max' => 'Foto harus kurang dari 10 MB',
    //     'pas_foto.mimes' => 'Foto harus berformat JPEG/JPG/PNG/PDF',
    //     'pas_foto.max' => 'Foto harus kurang dari 10 MB',
    //     'foto_ktp.mimes' => 'Foto harus berformat JPEG/JPG/PNG/PDF',
    //     'foto_ktp.max' => 'Foto harus kurang dari 10 MB'
    //     ]);

     	$file_permohonan = $request->file('srt_permohonan');
        $nama_file_permohonan = $file_permohonan->getClientOriginalName();
        $tujuan_upload_permohonan = 'foto';
        $file_permohonan->move($tujuan_upload_permohonan,$nama_file_permohonan);

        //SURAT PERMOHONAN KANWIL
        $file_kanwil = $request->file('srt_rekomendkanwil');
        $nama_file_kanwil = $file_kanwil->getClientOriginalName();
        $tujuan_upload_kanwil = 'foto';
        $file_kanwil->move($tujuan_upload_kanwil,$nama_file_kanwil);

        //SURAT KEPUTUSAN
        $file_keputusan = $request->file('srt_keputusan');
        $nama_file_keputusan = $file_keputusan->getClientOriginalName();
        $tujuan_upload_keputusan = 'foto';
        $file_keputusan->move($tujuan_upload_keputusan,$nama_file_keputusan);

        //SUSUNAN LEMBAGA 
        $file_susunan = $request->file('susunan_lembaga');
        $nama_file_susunan = $file_susunan->getClientOriginalName();
        $tujuan_upload_susunan = 'foto';
        $file_susunan->move($tujuan_upload_susunan,$nama_file_susunan);

        //PAS FOTO
        $file_pas = $request->file('pas_foto');
        $nama_file_pas = $file_pas->getClientOriginalName();
        $tujuan_upload_pas = 'foto';
        $file_pas->move($tujuan_upload_pas,$nama_file_pas);

        //FOTO KTP
        $file_ktp = $request->file('foto_ktp');
        $nama_file_ktp = $file_ktp->getClientOriginalName();
        $tujuan_upload_ktp = 'foto';
        $file_ktp->move($tujuan_upload_ktp,$nama_file_ktp);

	    DB::table('data_diri')->insert([
    	 'users_id' => $request->userid,
         'lembaga' => $request->lembaga,   
         'alamat' => $request->alamat,
         'kabupaten' => $request->kabupaten,
         'kota' => $request->kota,
         'provinsi' => $request->provinsi,
         'email' => $request->email,
         'no_hp' => $request->no_hp,
         'surat_permohonan' => $nama_file_permohonan,   
         'surat_rekomendkanwil' => $nama_file_kanwil,
         'surat_keputusan' => $nama_file_keputusan,
         'susunan_lembaga' => $nama_file_susunan,
         'pas_foto' => $nama_file_pas,
         'foto_ktp' => $nama_file_ktp
        ]);

        $User = new \App\User;
        $stat = "CHECKING";
        $make = $User::where('id', $request->userid)
        ->update(['status' => $stat]);

     	return redirect('/send');	
     }

     public function send(){

        return view('auth.notif.send');
     }

     public function hijau(){

        return view('auth.notif.hijau');
     }

     public function kuning(){

        return view('auth.notif.kuning');
     }

     public function merah(){

        return view('auth.notif.merah');
     }     

 }



