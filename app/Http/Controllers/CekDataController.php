<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CekDataModel;

class CekDataController extends Controller
{
   public function __construct()
   {
   	$this->CekDataModel = new CekDataModel();
   }

    public function index()
   {
      $data = [
         'cekdata' => $this->CekDataModel->allData(),
      ];
   	return view('dashboard.cekdata', $data);
   }

   public function detail($id)
   {
      // jika datanya tidak ada / kosong 
      if (!$this->CekDataModel->detailData($id)) {
         // maka akan menampilkan 404 NOT FOUND
         abort('404');
      }

      $data = [
         'cekdata' => $this->CekDataModel->detailData($id),
      ];
      return view('dashboard.detaildata', $data);
   }

     public function edit($id)
   {
       // jika datanya tidak ada / kosong 
      if (!$this->CekDataModel->detailData($id)) {
         // maka akan menampilkan 404 NOT FOUND
         abort('404');
      }

       $data = [
         'cekdata' => $this->CekDataModel->detailData($id),
      ];

      return view('dashboard.editdata', $data);
   }

    public function update($id)
   {
      //jika validasi input data nya tidak diisi atau kosong
      Request()->validate([
         'lembaga' => 'required',
        'alamat' => 'required',
        'kabupaten'     => 'required',
        'kota'    => 'required',
        'provinsi' => 'required',
        'email'   => 'required',
        'no_hp'   => 'required',
        'status' => 'required',
        'surat_permohonan' => 'mimes:jpeg,jpg,png,pdf|max:1024kb',
        'surat_rekomendkanwil' => 'mimes:jpeg,jpg,png,pdf|max:1024kb',
        'surat_keputusan' => 'mimes:jpeg,jpg,png,pdf|max:1024kb',
        'susunan_lembaga' => 'mimes:jpeg,jpg,png,pdf|max:1024kb',
        'pas_foto' => 'mimes:jpeg,jpg,png,pdf|max:1024kb',
        'foto_ktp' => 'mimes:jpeg,jpg,png,pdf|max:1024kb',

      ], [
         'lembaga.required' => 'wajib diisi !!',
         'alamat.required' => 'wajib diisi !!',
         'kabupaten.required' => 'wajib diisi !!',
         'kota.required' => 'wajib diisi !!',
         'provinsi.required' => 'wajib diisi !!',
         'email.required' => 'wajib diisi !!',
         'no_hp.required' => 'wajib diisi !!',
         'status' => 'wajib diisi !!',


      ]);
      // jika validasi tdk ada maka lakukan simpan data
      if (Request()->foto <> "") {
         // jika ingin ganti foto
         // upload gambar/foto
         $file = Request()->foto;
         $fileName = Request()->users_id. '.' . $file->extension();
         $file->move(public_path('foto'), $fileName);

         $data = [
            'lembaga' => Request()->lembaga,
            'alamat' => Request()->alamat,
            'kabupaten' => Request()->kabupaten,
            'kota' => Request()->kota,
            'provinsi' => Request()->provinsi,
            'email' => Request()->email,
            'no_hp' => Request()->no_hp,
            'surat_permohonan' => $fileName,
            'surat_rekomendkanwil' => $fileName,
            'surat_keputusan' => $fileName,
            'susunan_lembaga' => $fileName,
            'status' =>Request()->status,
            'pas_foto'  => $fileName,
            'foto_ktp' => $fileName,

         ];
         $this->CekDataModel->editData($id, $data);
      }else{
         // jika tidak ingin ganti foto
          $data = [
            'lembaga' => Request()->lembaga,
            'alamat' => Request()->alamat,
            'kabupaten' => Request()->kabupaten,
            'kota' => Request()->kota,
            'provinsi' => Request()->provinsi,
            'email' => Request()->email,
            'no_hp' => Request()->no_hp,
            'status' => Request()->status,
         ];
         $this->CekDataModel->editData($id, $data);
      }
      
      
      return redirect()->route('cekdata')->with('pesan', 'Data Berhasil Di Update !!!');
   }

}
