<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB; 

class AdminController extends Controller
{
    public function getAll(){
    	$cekdata = DB::table('data_diri')->join('users', 'users.id', '=', 'data_diri.users_id')->get();
    	return view('dashboard/cekdata',['cekdata' => $cekdata]); 
    }

    public function detailUser($id){
    	$cekdata = DB::table('data_diri')->join('users', 'users.id', '=', 'data_diri.users_id')->where('users_id', $id)->first();
    	return view('dashboard/detaildata',['cekdata' => $cekdata]); 
    }

    public function editdetailUser($id){
    	$cekdata = DB::table('data_diri')->join('users', 'users.id', '=', 'data_diri.users_id')->where('users_id', $id)->first();
    	return view('dashboard/editdata',['cekdata' => $cekdata]); 
    }

    public function updatedetailUser(Request $request){
    	$status = $request->status;
    	DB::table('users')->where('id',$request->ids)->update([
         'status' => $status
        ]);
    	return redirect('/admin/userList')->with('sukses', 'Status Berhasil Diubah');
    }

    public function deletedetailUser($id){
        DB::table('data_diri')->where('id_data_diri',$id)->delete();
        return redirect('/admin/userList')->with('sukses', 'Data Berhasil Dihapus');
    }


}