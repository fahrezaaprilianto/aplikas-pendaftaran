
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Konfirmasi &mdash; Dashboard Kebijakan</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="../node_modules/bootstrap-social/bootstrap-social.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('login/css/style.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>


<!-- Awal header -->
<div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
            <div class="login-brand">
              <img src="{{asset('login/img/logo.jpg')}}" alt="logo" width="200" class="shadow-light">
            </div>
            <!-- Akhir Header -->

            <!-- Awal body form -->
            <div class="card card-primary">
              <div class="card-header"><h4>Konfirmasi Berkas</h4></div>


              <div class="card-body">
                <form method="POST" action="/postConfirm" class="needs-validation" novalidate="" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <input type="hidden" name="userid" value="{{auth()->user()->id}}">            
                  
                  <div class="form-group">
                    <label for="nama">Nama Lembaga</label>
                    <input id="nama" type="text" class="form-control " name="lembaga" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="nama">Alamat</label>
                    <input id="nama" type="textarea" class="form-control " name="alamat" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="nama">Kabupaten</label>
                    <input id="nama" type="textarea" class="form-control " name="kabupaten" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="nama">Kota</label>
                    <input id="nama" type="textarea" class="form-control " name="kota" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="nama">Provinsi</label>
                    <input id="nama" type="textarea" class="form-control " name="provinsi" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input id="Email" type="Email" class="form-control " name="email" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="nama">No Tlp/Hp</label>
                    <input id="nama" type="number" class="form-control " name="no_hp" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="nama">Surat Permohonan</label>
                    <input id="nama" type="file" class="form-control " name="srt_permohonan" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="nama">Surat Rekomendasi Kanwil</label>
                    <input id="nama" type="file" class="form-control " name="srt_rekomendkanwil" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="nama">Surat Keputusan</label>
                    <input id="nama" type="file" class="form-control " name="srt_keputusan" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="nama">Susunan Pengurus Lenbaga</label>
                    <input id="nama" type="file" class="form-control " name="susunan_lembaga" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="nama">Pas Foto 4x6 Berwarna</label>
                    <input id="nama" type="file" class="form-control " name="pas_foto" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>

                  <div class="form-group">
                    <label for="nama">KTP Ketua, Sekertaris, Bendahara</label>
                    <input id="nama" type="file" class="form-control " name="foto_ktp" tabindex="1" value="" required autocomplete="off" autofocus>
                    
                                           
                  </div>






                  <div class="form-group">
                    <!-- <div class="form-check">
                      <input class="form-check-input" type="checkbox" name="remember" id="remember" >

                      <label class="form-check-label" for="remember">
                          Remember Me
                      </label>
                    </div> -->
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>          
                  </div>
                </form>
                <!-- <div class="text-center mt-4 mb-3">
                  <div class="text-job text-muted">Login With Social</div>
                </div>
                <div class="row sm-gutters">
                  <div class="col-6">
                    <a class="btn btn-block btn-social btn-facebook">
                      <span class="fab fa-facebook"></span> Facebook
                    </a>
                  </div>
                  <div class="col-6">
                    <a class="btn btn-block btn-social btn-twitter">
                      <span class="fab fa-twitter"></span> Twitter
                    </a>
                  </div>
                </div> -->

              </div>
            </div>
            <!-- Akhir body form -->

            <!-- <div class="mt-5 text-muted text-center">
              Don't have an account? <a href="register.html">Create One</a>
            </div> -->

            <!-- Awal footer -->
            <div class="simple-footer">
              Copyright &copy; Stisla 2018
            </div>
            <!-- Akhir footer -->
          </div>
        </div>
      </div>
    </section>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="{{asset('login/js/stisla.js')}}"></script>

  <!-- Template JS File -->
  <script src="{{asset('login/js/scripts.js')}}"></script>
  <script src="{{asset('login/js/custom.js')}}"></script>

  <!-- Page Specific JS File -->
  <script src="{{asset('login/js/auth-register.js')}}"></script>
</body>
</html>
