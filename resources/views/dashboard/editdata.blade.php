
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Dashboard &mdash; Edit Data</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('admin/css/components.css')}}">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            
          </ul>
          
        </form>
        <ul class="navbar-nav navbar-right">
         
          
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="{{asset('admin/img/avatar/avatar-1.png')}}" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, {{ Auth::user()->first_name}}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title">Logged in 5 min ago</div>
              <a href="features-profile.html" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <a href="features-activities.html" class="dropdown-item has-icon">
                <i class="fas fa-bolt"></i> Activities
              </a>
              <a href="features-settings.html" class="dropdown-item has-icon">
                <i class="fas fa-cog"></i> Settings
              </a>
              <div class="dropdown-divider"></div>
              <a href="/" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">Administrator</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">Dashboard</li>
              <li><a class="nav-link active" href="/dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
              <li class="menu-header">Starter</li>
             
              <li><a class="nav-link" href="/admin/userList"><i class="far fa-file"></i> <span>Cek Data Berkas</span></a></li>
             
            </ul>

          
        </aside>
      </div>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Edit Data Berkas</h1>
          </div>

          <div class="section-body">

            <div class="card">
                  <div class="card-header">
                    <h4>Data Berkas</h4>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="form-group col-md-6">
                        <label>Lembaga</label>
                        <input type="text" name="lembaga" value="{{ $cekdata->lembaga }}" class="form-control" readonly>
                      </div>

                      <div class="form-group col-md-6">
                        <label>Alamat</label>
                        <input type="text" name="alamat" value="{{ $cekdata->alamat }}" class="form-control" readonly>
                      </div>
                      
                      <div class="form-group col-md-6">
                        <label>Kabupaten</label>
                        <input type="text" name="kabupaten" value="{{ $cekdata->kabupaten }}" class="form-control" readonly>
                      </div>

                      <div class="form-group col-md-6">
                        <label>Kota</label>
                        <input type="text" name="kota" value="{{ $cekdata->kota }}"  class="form-control" readonly>
                      </div>

                      <div class="form-group col-md-6">
                        <label>Provinsi</label>
                        <input type="text" name="provinsi" value="{{ $cekdata->provinsi }}"  class="form-control" readonly>
                      </div>

                      <div class="form-group col-md-6">
                        <label>Email</label>
                        <input type="email" name="email" value="{{ $cekdata->email }}"  class="form-control" readonly>
                      </div>

                      <div class="form-group col-md-6">
                        <label>No. Hp</label>
                        <input type="number" name="no_hp" value="{{ $cekdata->no_hp }}"  class="form-control" readonly>
                      </div>

                      <div class="form-group col-md-6">
                        <label>Surat Permohonan</label>
                        <input type="file" name="surat_permohonan" class="form-control" readonly>
                      </div>

                      <div class="form-group col-md-6">
                        <label>Surat Rekomendasi Kanwil</label>
                        <input type="file" name="surat_rekomendwil" class="form-control" readonly>
                      </div>

                      <div class="form-group col-md-6">
                        <label>Surat Keputusan</label>
                        <input type="file" name="surat_keputusan" class="form-control" readonly>
                      </div>

                      <div class="form-group col-md-6">
                        <label>Susunan Lembaga</label>
                        <input type="file" name="susunan_lembaga" class="form-control" readonly>
                      </div>

                     
                        <div class="form-group col-md-6">
                          <div class="mb-2 text-muted">Pas Foto</div>
                        <div class="chocolat-parent">
                          <a href="{{url('foto/'.$cekdata->pas_foto)}}" class="chocolat-image" title="Just an example">
                        <div>
                          <img alt="image" src="{{url('foto/'.$cekdata->susunan_lembaga)}}" width="200px" class="img-fluid">
                        </div>
                      </a>
                        </div>
                      </div>

                       <div class="form-group col-md-6">
                          <div class="mb-2 text-muted">Foto KTP</div>
                        <div class="chocolat-parent">
                          <a href="{{url('foto/'.$cekdata->foto_ktp)}}" class="chocolat-image" title="Just an example">
                        <div>
                          <img alt="image" src="{{url('foto/'.$cekdata->susunan_lembaga)}}" width="200px" class="img-fluid">
                        </div>
                      </a>
                        </div>
                      </div>

                     <form action="/admin/userList/update/{{ $cekdata->id }}" method="POST">
                    {{csrf_field()}}
                      <div class="form-group col-md-12">
                        <label>Status</label>
                         <select name="status" class="form-control">
                            <option value="CHECKING" value="L" @if($cekdata->status == 'CHECKING') selected @endif>Berkas Sedang Dicheck</option>
                            <option value="HIJAU" @if($cekdata->status == 'HIJAU') selected @endif>Berkas Sudah Lengkap</option>
                            <option value="KUNING" @if($cekdata->status == 'KUNING') selected @endif>Berkas Lengkap namun ada data keliru</option>
                            <option value="MERAH" @if($cekdata->status == 'MERAH') selected @endif>Berkas Tidak Lengkap</option>
                          </select>
                          <input type="hidden" name="ids" value="{{$cekdata->id}}" class="form-control" readonly>
                      </div>

                    </div>
                      <button type="submit" class="btn btn-success">Update Data</button> 
                      <a href="/admin/userList" class="btn btn-info">Kembali</a>
                    </div>

                  </div>

                </div>

                </form>

          </div>
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer text-center">
          Copyright &copy; <?php echo date('Y') ?> <div class="bullet"></div> Design By <a href="https://nauval.in/">Anjani</a>
        </div>
        <div class="footer-right">
          2.3.0
        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="{{asset('admin/js/stisla.js')}}"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="{{asset('admin/js/scripts.js')}}"></script>
  <script src="{{asset('admin/js/custom.js')}}"></script>

  <!-- Page Specific JS File -->
</body>
</html>
