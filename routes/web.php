<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CekDataController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/','AuthController@login');
Route::get('/loginA','AdminController@loginA');
Route::post('/postLogin','AuthController@postLogin');
Route::post('/postLoginA','AdminController@postLoginA');
Route::get('/logout','AuthController@logout');
Route::get('/register','AuthController@register');
Route::post('/postRegister','AuthController@postRegister');

// ROUTE cek data 

// Route::get('/cekdata', [CekDataController::class, 'index'])->name('cekdata');
// Route::get('/cekdata/detail/{id}', [CekDataController::class, 'detail']);
// Route::get('/cekdata/edit/{id}', [CekDataController::class, 'edit']);
// Route::post('/cekdata/update/{id}', [CekDataController::class, 'update']);

Route::group(['middleware' => ['auth','checkRole:superadmin']],function(){
	Route::get('/dashboard','DashboardController@index');
	Route::get('/admin/approval','AdminController@approv');
	Route::get('/admin/userList','AdminController@getAll');
	Route::get('/admin/userList/detail/{id}', 'AdminController@detailUser');
	Route::get('/admin/userList/edit/{id}', 'AdminController@editdetailUser');
	Route::post('/admin/userList/update/{id}', 'AdminController@updatedetailUser');
	Route::get('/admin/userList/delete/{id}', 'AdminController@deletedetailUser');

});

Route::group(['middleware' => ['auth','checkRole:user,superadmin']],function(){
	Route::get('/confirm','AuthController@confirm');
	Route::get('/hijau','AuthController@hijau');
	Route::get('/kuning','AuthController@kuning');
	Route::get('/merah','AuthController@merah');
	Route::post('/postConfirm','AuthController@postConfirm');
	Route::get('/send','AuthController@send');
	Route::get('/dashboard','DashboardController@index');
});