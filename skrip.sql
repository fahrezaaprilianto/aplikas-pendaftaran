-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2021 at 01:01 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skrip`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_diri`
--

CREATE TABLE `data_diri` (
  `id_data_diri` int(100) NOT NULL,
  `users_id` varchar(255) NOT NULL,
  `lembaga` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kabupaten` varchar(255) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `provinsi` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `no_hp` varchar(255) NOT NULL,
  `surat_permohonan` varchar(255) NOT NULL,
  `surat_rekomendkanwil` varchar(255) NOT NULL,
  `surat_keputusan` varchar(255) NOT NULL,
  `susunan_lembaga` varchar(255) NOT NULL,
  `pas_foto` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_diri`
--

INSERT INTO `data_diri` (`id_data_diri`, `users_id`, `lembaga`, `alamat`, `kabupaten`, `kota`, `provinsi`, `email`, `no_hp`, `surat_permohonan`, `surat_rekomendkanwil`, `surat_keputusan`, `susunan_lembaga`, `pas_foto`, `foto_ktp`) VALUES
(2, '50', 'dwad', 'dwadaw', 'dawdw', 'adawda', 'dwadaw', 'dwadwa@gmail.com', '243242', '1.png', '1.png', '1.png', '1.png', '1.png', '1.png'),
(5, '52', 'sdsdefef', 'sdfffrf', 'ddfggg', 'sddffrf', 'dfvfvfv', 'anjhv@ggmail.com', '334354545', '48baznaz.jpg', '321-3217568_personal-skills-personal-skill-skills-logo.png', 'Budaya Organisasi Perusahaan PT.doc', 'alur-perumusan-RPJM.png', '12.jpg', '321-3217568_personal-skills-personal-skill-skills-logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `first_name`, `last_name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
(1, 'superadmin', 'roots', 'dwadwad', 'root@gmail.com', NULL, '$2y$10$qB.Z9cmPvFBZ5qxQjt6TBOSxsdz3HUpEn.Mf//yurEQ4Lpm/pzxGW', 'V4Jmi309LPQEJZhKP6qy6evKlBrBcoYFIYsmZsbqmxyjIE6HNRlFGiUXIiPQ', '2020-10-12 10:04:47', '2021-06-02 00:38:02', 'tes'),
(50, 'user', 'rudi', 'purwanto', 'rudi@gmail.com', NULL, '$2y$10$qB.Z9cmPvFBZ5qxQjt6TBOSxsdz3HUpEn.Mf//yurEQ4Lpm/pzxGW', 'IuagBhcTWwC6niPy1np3L6uanczWHG7cTyCUDlDyJptk93YIHtu2HxAeRgVU', '2021-07-16 06:00:06', '2021-07-17 04:50:54', 'HIJAU'),
(51, 'superadmin', 'admin', 'admin', 'admin@gmail.com', NULL, '$2y$10$IxusWLrVyBRzcVoY9LaGfeApO8YUmjXgww2FOSXJsRV0y88ImRNrO', 'kXYX8lYXOeQZCIsqMlBcr9HdVs5MoVK7EJaric879L7TyLQrLV0p3jrNAMSS', '2021-07-17 06:30:35', '2021-07-17 06:30:35', 'dad'),
(52, 'user', 'shara', 'fira', 'safira@gmail.com', NULL, '$2y$10$ThrCfK9r5A.ggTf/jXkkFetzY7E9l.lQU6dibW0U1v1Zw9sqlyr3.', '5XtRJSsGhisDhCAyh8eeiN5mI8jasfPSQawLWAQuwtgoUtudXSo8mYnB1odR', '2021-07-18 04:10:22', '2021-07-24 05:07:16', 'KUNING'),
(53, 'user', 'anjany', 'vicky', 'jny0059@gmail.com', NULL, '$2y$10$EFHN3hDObXmGcX//MGlble6KxFZksaePwusU3RfX0YHYxADIeoEZe', 'eOjTlsPXkuukicfy8fe4GvRvs4UNcinItZWPnBt1in7DNUbUUob2wankPH0t', '2021-07-24 04:18:10', '2021-07-24 04:18:10', 'NOT_CONFIRM');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_diri`
--
ALTER TABLE `data_diri`
  ADD PRIMARY KEY (`id_data_diri`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_diri`
--
ALTER TABLE `data_diri`
  MODIFY `id_data_diri` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
